# Экран "Прикрепления"

Эта часть словаря содержит сведения об экране “прикрепления”.

## Файлы

<glossary-variable color="green">

### files_folderIcon

Задает цвет иконки папки.

</glossary-variable>

<glossary-variable color="red">

### files_folderIconBackground

Задает цвет заднего фона иконки папки.

</glossary-variable>

<glossary-variable color="blue">

### files_iconText

Задает цвет текста внутри иконок файлов.

</glossary-variable>

<figure>

![](./images/attachscreens.0.png)

<figcaption>

Зеленое — `files_folderIcon`, красное — `files_folderIconBackground`, синее —
`files_iconText`.

</figcaption>
</figure>

## Геопозиция

<glossary-variable color="red">

### location_markerX

Задает цвет × под иконкой местополежения, когда вы двигаете её по карте.

</glossary-variable>

<glossary-variable color="green">

### location_sendLocationBackground

Задает цвет фона кнопки рядом с “Отправить свою геопозицию”.

**Важно:** вы должны перезайти на экран отправки геопозиции, чтобы увидеть цвет
кнопки, если изменили его с помощью редактора в приложении.

</glossary-variable>

<glossary-variable color="blue">

### location_sendLocationIcon

Задает цвет иконки внутри кнопки.

</glossary-variable>

<figure>

![](./images/attachscreens.1.png)

<figcaption>

Красное — `location_markerX`, зеленое — `location_sendLocationBackground`, синее
— `location_sendLocationIcon`.

</figcaption>
</figure>

## Музыка

<glossary-variable color="blue">

### musicPicker_buttonBackground

Задает цвет фона музыкального файла.

</glossary-variable>

<glossary-variable color="yellow">

### musicPicker_buttonIcon

Задает цвет иконки `musicPicker_buttonBackground`.

</glossary-variable>

<glossary-variable color="green">

### musicPicker_checkbox

Задает цвет фона метки выбранного файла.

</glossary-variable>

<glossary-variable color="red">

### musicPicker_checkboxCheck

Задает цвет иконки внутри `musicPicker_checkbox`.

</glossary-variable>

<figure>

![](./images/attachscreens.2.png)

<figcaption>

Blue — `musicPicker_buttonBackground`, yellow — `musicPicker_buttonIcon`, green
— `musicPicker_checkbox`, red — `musicPicker_checkboxCheck`.

</figcaption>
</figure>

<glossary-variable color="blue">

### picker_badge

Задает цвет фона иконки счетчика отправки.

</glossary-variable>

<glossary-variable color="yellow">

### picker_badgeText

Задает цвет текста иконки `picker_badge`.

</glossary-variable>

<glossary-variable color="green">

### picker_disabledButton

Задает цвет кнопки “Отправить”, когда ничего не выбрано.

</glossary-variable>

<glossary-variable color="red">

### picker_enabledButton

Задает цвет кнопки “Отправить”, когда выбраны файлы.

</glossary-variable>

<figure>

![](./images/attachscreens.3.png)

<figcaption>

Зеленое — `picker_badge`, желтое — `picker_badgeText`, красное —
`picker_disabledButton`, синее — `picker_enabledButton`.

</figcaption>
</figure>
