# Attach Screens

This part of glossary is about attach screens.

## Files

<glossary-variable color="green">

### files_folderIcon

Sets the folder icon.

</glossary-variable>

<glossary-variable color="red">

### files_folderIconBackground

Sets the circle background below the folder icon.

</glossary-variable>

<glossary-variable color="blue">

### files_iconText

Sets the file icon extension text.

</glossary-variable>

<figure>

![](./images/attachscreens.0.png)

<figcaption>

Green — `files_folderIcon`, red — `files_folderIconBackground`, blue —
`files_iconText`.

</figcaption>
</figure>

## Location

<glossary-variable color="red">

### location_markerX

Sets the color of the × below the red location icon when you move it across the
map.

</glossary-variable>

<glossary-variable color="green">

### location_sendLocationBackground

Sets the background of the button beside the “Send selected/your current
location”.

**Important note:** you must re-enter the screen to see the actual color of the
button if you change the variable with the in-app editor.

</glossary-variable>

<glossary-variable color="blue">

### location_sendLocationIcon

Set the icon on the button.

</glossary-variable>

<figure>

![](./images/attachscreens.1.png)

<figcaption>

Red — `location_markerX`, green — `location_sendLocationBackground`, blue —
`location_sendLocationIcon`.

</figcaption>
</figure>

## Music

<glossary-variable color="blue">

### musicPicker_buttonBackground

The background of the icon from your music gallery.

</glossary-variable>

<glossary-variable color="yellow">

### musicPicker_buttonIcon

The icon music inside `musicPicker_buttonBackground`.

</glossary-variable>

<glossary-variable color="green">

### musicPicker_checkbox

The background of the check on the selected music.

</glossary-variable>

<glossary-variable color="red">

### musicPicker_checkboxCheck

The check icon inside musicPicker_checkbox.

</glossary-variable>

<figure>

![](./images/attachscreens.2.png)

<figcaption>

Blue — `musicPicker_buttonBackground`, yellow — `musicPicker_buttonIcon`, green
— `musicPicker_checkbox`, red — `musicPicker_checkbox`.

</figcaption>
</figure>

<glossary-variable color="blue">

### picker_badge

The background of the send counter.

</glossary-variable>

<glossary-variable color="yellow">

### picker_badgeText

The number inside the circle.

</glossary-variable>

<glossary-variable color="green">

### picker_disabledButton

The color of the “send” button when you haven't picked anything yet.

</glossary-variable>

<glossary-variable color="red">

### picker_enabledButton

the color of the “send” button when you have picked items.

</glossary-variable>

<figure>

![](./images/attachscreens.3.png)

<figcaption>

Green — `picker_badge`, yellow — `picker_badgeText`, red —
`picker_disabledButton`, blue — `picker_enabledButton`.

</figcaption>
</figure>
